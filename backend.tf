terraform {
  # required_version = "~>1.5.6"
  backend "gcs" {
    credentials = "./creds/gcp.json"
    bucket = "qstd_tfstate"
  }
}