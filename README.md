# qstd-devops-test



## Create CI/CD Pipelines for Terraform in GitLab

### This project will create:

- Virtual Private Cloud (VPC) with private and public subnets.
- Two Compute Engine instances: one in the public subnet and one in the private subnet.
- Nginx installed and running on the Compute Engine instance in the public subnet, serving a static HTML page.
- The Compute Engine instance in the private subnet should not have direct internet access.

new line
