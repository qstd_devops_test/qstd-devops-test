
resource "google_compute_instance" "public_instance" {
  name         = "public-instance"
  machine_type = "e2-micro"
  zone         = "us-central1-c"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
      labels = {
        my_label = "value"
      }
    }
  }

  network_interface {
    network    = google_compute_network.vpc_network.self_link
    subnetwork = google_compute_subnetwork.public_subnet.self_link

    access_config {}
  }

  metadata_startup_script = <<-EOT
    #!/bin/bash
    apt-get update
    apt-get install -y nginx
    echo '<html><head><title>Thank you for the opportunity to participate in the process</title></head><body><h1>I really hope to be able to work with you soon!!</h1></body></html>' > /var/www/html/index.html
    systemctl enable nginx
    systemctl start nginx
    EOT

  tags = ["nginx"]
}

resource "google_compute_instance" "private_instance" {
  name         = "private-instance"
  machine_type = "e2-micro"
  zone         = "us-central1-c"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
      labels = {
        my_label = "value"
      }
    }
  }

  network_interface {
    network    = google_compute_network.vpc_network.self_link
    subnetwork = google_compute_subnetwork.private_subnet.self_link
  }
}