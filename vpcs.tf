resource "google_compute_network" "vpc_network" {
  project                 = "qstd-nginx"
  name                    = "vpc-network"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "public_subnet" {
  name          = "public-subnet"
  ip_cidr_range = "10.0.1.0/24"
  region        = "us-central1"
  network       = google_compute_network.vpc_network.id
  project       = "qstd-nginx"

  secondary_ip_range {
    range_name    = "private-ip-range"
    ip_cidr_range = "10.0.2.0/24"
  }
}

resource "google_compute_subnetwork" "private_subnet" {
  name          = "private-subnet"
  ip_cidr_range = "10.0.3.0/24"
  network       = google_compute_network.vpc_network.id
  region        = "us-central1"
}