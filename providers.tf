locals {
  project = "qstd-nginx"
  region  = "us-central1"
  zone    = "us-central1-c"
}

provider "google" {
  credentials = "./creds/gcp.json"

  project = local.project
  region  = local.region
  zone    = local.zone
}
